﻿using System;
using Autofac;

namespace DIContainerEx
{
    public class Employee
    {
        
    }
    interface IEmployeeService
    {
        Employee GetEmployee();
    }

    interface IPrintService
    {
        void Print(Employee employee);
    }

    class PrintService : IPrintService
    {

        public PrintService()
        {
        }

        public void Print(Employee employee)
        {
        }
    }
    
    class EmployeeService : IEmployeeService
    {
        protected readonly IPrintService _printService;

        public EmployeeService(IPrintService printService)
        {
            _printService = printService; //injected
        }

        public Employee GetEmployee()
        {
            return new Employee();
        }
    }
    
    class Application
    {
        protected readonly IEmployeeService _employeeService;
        protected readonly IPrintService _printService;

        public Application(IEmployeeService employeeService, IPrintService printService)
        {
            _employeeService = employeeService; //Injected
            _printService = printService; //Injected
        }

        public void Run()
        {
            var employee = _employeeService.GetEmployee();
            _printService.Print(employee);
        }
    }

    class Program
    {
        private static IContainer CompositionRoot()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Application>();
            builder.RegisterType<EmployeeService>().As<IEmployeeService>();
            builder.RegisterType<PrintService>().As<IPrintService>();
            return builder.Build();
        }
        
        static void Main(string[] args)
        {
            var app = CompositionRoot().Resolve<Application>();
            app.Run();
            
            Console.WriteLine("Hello World!");
        }
    }
}