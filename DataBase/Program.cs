﻿using System;
using System.Linq;
using DataBase.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace DataBase
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                db.Words.Add(new Word(){ NameEng = "Car", NameRus = "Машина" });
                db.SaveChanges();
                foreach (var word in db.Words)
                {
                    Console.WriteLine(word.NameEng + ":" + word.NameRus);
                }

                Console.WriteLine("Raw sql");
                var result = db.Words.FromSqlRaw("Select * from Words where Id > 2").ToList();
                foreach (var word in result)
                {
                    Console.WriteLine(word.NameEng + ":" + word.NameRus);
                }
            }


            Console.WriteLine("================ ADO.NET ===================");

            string connectionString = @"Server=(localdb)\mssqllocaldb;Database=MyTestDataBaseProject;Trusted_Connection=True;";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = "SELECT * FROM Words where Id > 2";
                command.Connection = connection;
                var reader = command.ExecuteReader();
                
                if(reader.HasRows) 
                {
                    Console.WriteLine("{0}\t{1}\t{2}", reader.GetName(0), reader.GetName(1), reader.GetName(2));
 
                    while (reader.Read())
                    {
                        object id = reader.GetValue(0);
                        object name = reader.GetValue(1);
                        object age = reader.GetValue(2);
 
                        Console.WriteLine("{0} \t{1} \t{2}", id, name, age);
                    }
                }
         
                reader.Close();
            }
            
            Console.WriteLine("Finish");
            
        }
    }
}