﻿using DataBase.Models;
using Microsoft.EntityFrameworkCore;

namespace DataBase
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Word> Words { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var sqlConnectionString = @"Server=(localdb)\mssqllocaldb;Database=MyTestDataBaseProject;Trusted_Connection=True;";
            optionsBuilder.UseSqlServer(sqlConnectionString);
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new WordConfiguration());
        }
    }
}