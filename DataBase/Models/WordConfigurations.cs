﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBase.Models
{
    public class WordConfiguration : IEntityTypeConfiguration<Word>
    {
        public void Configure(EntityTypeBuilder<Word> builder)
        {
            builder.ToTable("Words").HasKey(p => p.Id);
            builder.Property(p => p.NameRus).IsRequired().HasMaxLength(100);
            builder.Property(p => p.NameEng).IsRequired().HasMaxLength(100);
        }
    }
}