﻿namespace DataBase.Models
{
    public class Word
    {
        public int Id { get; set; }
        public string NameRus { get; set; }
        public string NameEng { get; set; }
    }
}