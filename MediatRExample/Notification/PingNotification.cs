﻿using System;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace MediatRExample.Notification
{
        public class PingNotify : INotification { }
        
        public class Pong1 : INotificationHandler<PingNotify>
        {
            public Task Handle(PingNotify notification, CancellationToken cancellationToken)
            {
                Console.WriteLine("Pong 1");
                return Task.CompletedTask;
            }
        }

        public class Pong2 : INotificationHandler<PingNotify>
        {
            public Task Handle(PingNotify notification, CancellationToken cancellationToken)
            {
                Console.WriteLine("Pong 2");
                return Task.CompletedTask;
            }
        }
}