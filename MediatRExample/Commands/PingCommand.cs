﻿using System;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace MediatRExample.Commands
{
    public class PingCommand : IRequest<string>
    {
    }
    
    public class PingHandler : IRequestHandler<PingCommand, string>
    {
        public Task<string> Handle(PingCommand request, CancellationToken cancellationToken)
        {
            Console.WriteLine("Ping Command - Ok");
            return Task.FromResult("Pong");
        }
    }
    
}